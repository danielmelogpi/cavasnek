export const tick = (step) => {
    window.requestAnimationFrame((t) => {
        step(t)
        setImmediate(() => tick(step))
    })
}

export const timeline = (step) => {
    tick(step)
}