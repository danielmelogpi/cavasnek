
import {directionEnum, computeCartesianMove} from './move-vector'
import { setState, getState } from './state';
const directionMap = {
    37: directionEnum.LEFT,
    38: directionEnum.UP,
    39: directionEnum.RIGHT,
    40: directionEnum.DOWN
}
const opositeDirection = {
    [directionEnum.LEFT]: directionEnum.RIGHT,
    [directionEnum.RIGHT]: directionEnum.LEFT,
    [directionEnum.UP]: directionEnum.DOWN,
    [directionEnum.DOWN]: directionEnum.UP,
}
export const initKeyboard = () => {
    window.onkeydown = (event) => {
        let {speed, lastDirection} = getState()
        let dir = directionMap[event.keyCode]
        if (!dir) return
        if (dir == opositeDirection[lastDirection]) return
        let cartesianMove = computeCartesianMove(dir, speed)
        setState({
            cartesianMove,
            lastDirection: dir
        })
    }
}