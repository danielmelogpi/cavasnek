import { PalleteDefault } from "./pallete";

const rectangle = (ctx, {x, y}, {w, h}, {fill}) => {
    ctx.fillStyle = fill || 'black';
    ctx.fill = fill
    ctx.fillRect(x,y,w,h)
}

const clear = (ctx, {stageWidth, stageHeight}) => {
    ctx.clearRect(0, 0, stageWidth, stageHeight);
}

export const CreateDrawer = (ctx, {stageWidth, stageHeight}) => {
    return {
        clear: () => clear(ctx, {stageWidth, stageHeight}),
        rectangle: ({x, y = x}, {w, h = w}, {fill} = {fill: PalleteDefault.fill}) => {
            rectangle(ctx, {x, y}, {w, h}, {fill})
        },
    }
}