export const directionEnum = {
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
    UP: 'UP',
    DOWN: 'DOWN'
}

export const computeCartesianMove = (dir, baseFactor) => {
    if (dir == directionEnum.UP) {
        return {x: 0, y: -1 * baseFactor}
    }
    if (dir == directionEnum.DOWN) {
        return {x: 0, y: baseFactor}
    }
    if (dir == directionEnum.LEFT) {
        return {x: -1 * baseFactor, y: 0}
    }
    if (dir == directionEnum.RIGHT) {
        return {x: baseFactor, y: 0}
    }
}

export const applyCartesianMove = (cartesianMove, {x, y}) => {
    return {
        x: x + cartesianMove.x,
        y: y + cartesianMove.y
    }
}