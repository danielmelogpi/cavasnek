let initialSpeed = 1
let store = {
    speed: initialSpeed,
    rect: {
        x: 1,
        y: 1
    },
    cartesianMove: {
        x: initialSpeed,
        y: 0
    },
    food: {
        created: false,
        eaten: false,
        x: 0,
        y: 0
    },
    snekBody: [{x: 1, y: 1}, {x: 2, y: 1}, {x: 3, y: 1}, {x: 4, y: 1}]
}

export const setState = (incomming) => {
    store = {
        ...store,
        ...incomming
    }
}
export const getState = () => {
    return {...store}
}