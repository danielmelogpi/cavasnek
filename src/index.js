import * as styles from './styles.css'
import { CreateDrawer } from './snek/draw';
import { timeline } from './snek/timeline';
import { getState, setState } from './snek/state'
import { initKeyboard } from './snek/keyboard';
import { applyCartesianMove } from './snek/move-vector';
import { Collors } from './snek/pallete';

const GRID_MAX = 25
window.onload = () => {
    let stageCanvas = document.querySelector('#game-stage')
    var gameContext = stageCanvas.getContext('2d')
    const Drawer = CreateDrawer(gameContext, {stageWidth: 600, stageHeight: 600})
    initKeyboard()
    timeline((timestamp) => tick(Drawer, timestamp))
}

const tick = (Drawer, frametimestap) => {
    Drawer.clear()
    let {rect, cartesianMove} = getState()
    let nextCoords = {
        rect: applyCartesianMove(cartesianMove, rect)
    }
    checkFoodEaten()
    setState(nextCoords)
    renderFood(Drawer)
    renderSnek(Drawer)
    // Drawer.rectangle({x:nextCoords.rect.x, y: nextCoords.rect.y}, {w:20})
}

const renderFood = (Drawer) => {
    let {food: {created, eaten, x, y}} = getState()
    if (created && !eaten) {
        Drawer.rectangle(slotCoordPair({x, y}), {w: 20} )
    } else {
        let newFoodCoords = createFoodCoord()
        setState({
            food: {
                created: true,
                eaten: false,
                x: newFoodCoords.x,
                y: newFoodCoords.y
            }
        })
        Drawer.rectangle(slotCoordPair({x:newFoodCoords.x, y: newFoodCoords.y}), {w: 5} , Collors.RED)
    }
}

const renderSnek = (Drawer) => {
    let {snekBody, cartesianMove} = getState()
    let head = snekBody[snekBody.length - 1]
    let newBody = snekBody.map((point, i) => {
        return snekBody[i+1] || applyCartesianMove(cartesianMove, head)
    })
    let newHead = newBody[newBody.length - 1]
    if (newHead.x > GRID_MAX || newHead.y > GRID_MAX || newHead.y < 0 || newHead.x < 0) {
        newBody = snekBody
    }
    newBody.forEach(point => Drawer.rectangle({x:slotCoord(point.x), y: slotCoord(point.y)}, {w:20}))
    setState({
        snekBody: newBody
    })
}

const checkFoodEaten = () => {
    let {snekBody, food} = getState()
    let mapSpread = {x: 128, y: 256}
    let head = snekBody[snekBody.length - 1]
    let headCoords = head.x * mapSpread.x + head.y * mapSpread.y
    let foodCoords = food.x * mapSpread.x + food.y * mapSpread.y
    if (headCoords == foodCoords) {
        console.log(headCoords, (foodCoords))
        setState({
            food: {
                ...food,
                eaten: true
            }
        })
    }
}

const createFoodCoord = () => {
    let {snekBody} = getState()
    let mapSpread = {x: 128, y: 256}
    let occupiedCoords = snekBody.map(({x, y}) => x * mapSpread.x + y * mapSpread.y)
    while(true) {
        let randomX = Math.floor(Math.random() * GRID_MAX) + 1
        let randomY = Math.floor(Math.random() * GRID_MAX) + 1
        let occupied = occupiedCoords.indexOf(randomX * mapSpread.y + randomX + mapSpread.y) >=0
        if (!occupied) {
            return {x: randomX, y: randomY}
        }
    }
    
    
}
const slotCoordPair = ({x, y}) => ({x: slotCoord(x), y: slotCoord(y)})
const slotCoord = (c) => c * 23